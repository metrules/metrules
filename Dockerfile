FROM python:3.8-slim as base

ENV RETRORULES_NAME=retrorules_rr02_rp3_hs
ENV RETRORULES_DIR=/srv/$RETRORULES_NAME
ENV METRULES_RULES_PATH=$RETRORULES_DIR/retrorules_rr02_flat_all.tsv 
ENV METRULES_CFMID_PATH=/opt/cfm_id

FROM base as build

WORKDIR /app

COPY vendor/retrorules/$RETRORULES_NAME.tar.gz .
RUN tar xzf $RETRORULES_NAME.tar.gz -C /srv

ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.1.6

RUN pip install "poetry==$POETRY_VERSION"
COPY pyproject.toml poetry.lock ./
RUN poetry export -f requirements.txt --without-hashes > /root/requirements.txt

FROM base as final

ENV APP_DIR=/app
ENV METRULES_PACKAGE_DIR=$APP_DIR/metrules

RUN apt-get update && \
    apt-get install --no-install-recommends -y libpango1.0  && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir $APP_DIR

WORKDIR $APP_DIR

COPY vendor/cfm_id $METRULES_CFMID_PATH
COPY --from=build $RETRORULES_DIR $RETRORULES_DIR

COPY --from=build /root/requirements.txt $APP_DIR
RUN pip3 install --no-cache-dir -r requirements.txt 

COPY metrules $METRULES_PACKAGE_DIR
COPY run.py run.py

COPY pyproject.toml pyproject.toml
RUN pip3 install $APP_DIR

ENV SINAGOT_LOGGING_LEVEL=WARNING
ENV MPLCONFIGDIR=/etc/matplotlib
RUN mkdir $MPLCONFIGDIR && chmod a+rw $MPLCONFIGDIR

CMD  ["python", "run.py", "/data"]