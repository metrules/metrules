# MetRules

MetRules extends the _Metabolism Based Propagation_ workflow introduced by [MetWork](http://www.metwork.science)
by using [RetroRules](https://retrorules.org/) 30k+ reaction rules extracted from public databases.

Compare to MetWork, MetRules enables to explore and assess putative annotations from an annotated MS/MS extract
without the need of manually create or choose reaction rules.
It's faster to setup for increased exploration capacity.
However, MetRules lacks some feature of MetWork has and is currently only available as a Docker image or Python source code.

Assessments are done with [CFM-ID](https://cfmid.wishartlab.com/) 2.0 _in silico_ fragmentation and [Matchms](https://matchms.readthedocs.io/en/latest/) similarity cosine.

## Usage

### Prepare data

You have to put your inputs data in a folder with the following structure ([...] can be any file name) :

```
└── inputs/
    └── [spectrum_set].mgf
    └── [annotations].csv
```

`[spectrum_set].mgf` (you ) is your MS/MS data in `.mgf` format with a required `SCANS=` metadata for each ions.

For example :

```
BEGIN IONS
PEPMASS=239.163190
SCANS=1
105.046028 29.035715
106.778130 6.062500
109.104729 25.778847
```

`[annotations].csv` is your initial annotation of `[spectrum_set].mgf` data in `.csv` format with the required first 3 columns :

```
IonId,name,smiles
119,Pericyclivine,C/C=C1/CN2[C@H]3C[C@@H]1[C@@H](C(=O)OC)[C@@H]2Cc1c3[nH]c2ccccc12
24,Splendoline,C[C@H]1OC[C@H]2[C@@H]3N(C(=O)CO)c4ccccc4[C@@]34CCN3C[C@@H]1[C@@H]2C[C@]34O
```

You can add as many other metadata columns you need, so you can directily use MetWork annotation files.

You can download this [example dataset](https://gitlab.com/metrules/metrules/-/archive/v0.1.3/metrules-v0.1.3.zip?path=tests/data/CANPA_sample/tested_data) extracted from [CANPA publication](https://pubs.acs.org/doi/10.1021/acs.analchem.9b02216).

### Install Docker

The recommended way to use MetRules is to run the Docker image `yannbeauxis/metrules`.

First you need to install [Docker](https://www.docker.com/get-started).
If your on Windows or Mac, install "Docker Desktop".

For Windows users, it's also recommanded to use [PowerShell](https://docs.microsoft.com/fr-fr/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.2#msi) to run docker command lines. You should also disable legacy console with the following steps :

1. Close Docker Desktop
2. Open a PowerShell
3. Right click or press and hold on the title bar of the console window, and click/tap on Properties
4. Click/tap on the Options tab, uncheck the Use legacy console box and click/tap on OK to apply
5. Start Docker Desktop

### Run Docker image

Once installed, run the following command line (change the `/path/to/local/data` value with the path to the data you want to process).

For **Mac and Linux** users:

```
docker run \
    --rm \
    --shm-size=5.03gb \
    -u $(id -u ${USER}):$(id -g ${USER}) \
    -e METRULES_DIAMETER=14 \
    -e METRULES_COSINE_THRESHOLD=0.3 \
    -v /path/to/local/data:/data \
    yannbeauxis/metrules:latest
```

For **Windows** users:

```
docker run `
    --rm `
    --shm-size=5.03gb `
    -e METRULES_DIAMETER=14 `
    -e METRULES_COSINE_THRESHOLD=0.3 `
    -v /path/to/local/data:/data `
    yannbeauxis/metrules:latest
```

You can replace `14` of `METRULES_DIAMETER=14` with another even value.
Diameter set the rules substrate specificity from 2 (lowest specificity) to 16 (highest specificity).
You can begin with a high diameter and the try lower diameters if no match.
See [RetroRules publication](https://academic.oup.com/nar/article/47/D1/D1229/5128930?login=false) for more information about diameter.

You can replace `0.3` of `METRULES_COSINE_THRESHOLD=0.3` with another float value to change cosine threshold for assessment.

### Analyze results

Once the run finished, an `Assessment Report - D*.pdf` has been created on a `reports` directory on your data folder.
You can click on inchi keys and EC numbers to access to web ressources from [Lotus](https://lotus.naturalproducts.net/) and [Brenda](https://www.brenda-enzymes.org/index.php) or [MetaNetX](https://www.metanetx.org/databases).

For example with the CANPA sample dataset :

<img src="docs/report.png" width="500">
