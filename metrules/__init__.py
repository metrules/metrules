import pkg_resources

from .workspace import MetRulesWorkspace

__version__ = pkg_resources.get_distribution("metrules").version

__all__ = ["MetRulesWorkspace"]
