from pathlib import Path
from dataclasses import dataclass
from typing import Generator, Union

import pandas as pd
from rdkit import Chem
import sinagot as sg


@dataclass
class AnnotationRow:
    ion_id: str
    name: str
    smiles: str

    @property
    def rd_mol(self) -> Chem.rdchem.Mol:
        return Chem.MolFromSmiles(self.smiles)

    @property
    def inchi_key(self) -> str:
        return str(Chem.MolToInchiKey(self.rd_mol))


def read_annotations(path: Union[str, Path]) -> pd.DataFrame:
    df = pd.read_csv(
        path,
        header=0,
        dtype="str",
    )
    names = ("ion_id", "name", "smiles")
    df = df.rename(columns={df.columns[idx]: name for idx, name in enumerate(names)})
    return df


class AnnotationRowHandler(sg.storage.TypeHandler, data_type=AnnotationRow):
    def read(self, path: Path) -> AnnotationRow:
        df = read_annotations(path)
        df = df[df.ion_id == self.workflow_id]
        return AnnotationRow(*df.iloc[0].iloc[[0, 1, 2]])


class LocalAnnotationStorage(sg.LocalStorage):
    def iter_workflow_ids(
        self, workspace: sg.base.WorkspaceBase
    ) -> Generator[str, None, None]:
        path = str(workspace._resolve_path(self))
        df = read_annotations(path)
        yield from df.ion_id
