from typing import List, Any, Dict
from dataclasses import field
from pathlib import Path
import json
import re

from pydantic.dataclasses import dataclass
import pandas as pd
import sinagot as sg
from rdkit import Chem
from rdkit.Chem.Draw import rdDepictor
from md2pdf.core import md2pdf
from matchms.filtering import normalize_intensities, select_by_intensity
from matchms_plotly import plot_spectra
from cfm_id.matchms import load_from_cfm_id

import metrules
from metrules.logger import logger
from .settings import settings
from .workflow import MetRulesWorkflow
from .annotation import read_annotations
from .template import TemplateEngine

rdDepictor.SetPreferCoordGen(True)

EC_MATCHED_PATTERN = r"\d+\.\d+\.\d+\.\d+"


@dataclass
class Rule:
    rule_id: str
    smarts: str
    reaction_id: str
    ec_number: str
    score_normalized: float
    images_path: Path

    @property
    def image_name(self) -> str:
        return f"rule-{self.rule_id}.svg"

    @property
    def image_path(self) -> str:
        return f"images/{self.image_name}"

    @property
    def ec_number_match(self) -> bool:
        return bool(re.match(EC_MATCHED_PATTERN, self.ec_number))


@dataclass
class Annotation:
    ion_id: str
    smiles: str
    images_path: Path

    @property
    def image_name(self) -> str:
        return f"{self.inchi_key}.svg"

    @property
    def mol_image_path(self) -> str:
        return f"images/{self.image_name}"

    @property
    def spectra_image_path(self) -> str:
        return f"images/spectra-{self.ion_id}-{self.inchi_key}.svg"

    @property
    def inchi_key(self) -> str:
        return str(Chem.MolToInchiKey(Chem.MolFromSmiles(self.smiles)))


@dataclass
class Source(Annotation):
    name: str


@dataclass
class Candidate(Annotation):
    cosine: float
    best_rule: Rule
    best_source: Source


@dataclass
class Target:
    ion_id: str = ""
    mass: float = 0
    mz: float = 0
    candidates: List[Candidate] = field(default_factory=list)


class Assessment:
    def __init__(self, workspace: sg.Workspace[MetRulesWorkflow]):
        self.workspace = workspace
        self._report_dataframe = None

    def gen_report_csv(self, force: bool = False) -> pd.DataFrame:
        csv_report_path = self.report_path(".csv")
        if (not csv_report_path.exists()) or force:
            df = self.report_dataframe()
            if settings.report_csv:
                df.to_csv(csv_report_path, index=False)
        else:
            df = pd.read_csv(csv_report_path)
        return df

    def gen_report_pdf(self, force: bool = False) -> None:
        pdf_path = self.report_path(".pdf")

        if (not pdf_path.exists()) or force:
            md_content = self.gen_report_md()
            if md_content:
                md2pdf(
                    str(pdf_path),
                    md_content=md_content,
                    css_file_path=str(
                        TemplateEngine().template_dir_path / "assessment-report.css"
                    ),
                    base_url=str(self.report_dir),
                )

    def gen_report_md(self, force: bool = False) -> str:
        md_path = self.report_path(".md")

        if (not md_path.exists()) or force:
            targets = self.get_targets()
            if not targets:
                return ""
            candidates_row = self.get_candidates_rows()
            candidates_total = self.get_candidates_total()

            md_content = TemplateEngine().gen_md_content(
                template_name="assessment-report",
                metrules_version=metrules.__version__,
                targets=targets,
                settings=settings,
                candidates_row=candidates_row,
                candidates_total=candidates_total,
            )
            if settings.report_md:
                md_path.write_text(md_content)

        return md_content

    @property
    def report_dir(self) -> Path:
        return self.workspace.root_path / settings.reports_folder

    def report_path(self, suffix: str) -> Path:
        stem = self.report_dir / f"Assessment Report - D{settings.diameter}"
        return stem.with_suffix(suffix)

    def get_candidates_rows(self) -> Any:
        df = self.report_dataframe()
        df["product_mz_round"] = round(df.product_mz, 4)
        return (
            df.groupby(["product_mz_round", "product_ion_id"])
            .agg(
                {
                    "product_inchi_key": "nunique",
                    "substrate_ion_id": "nunique",
                    "cosine": "max",
                    "rule_score_normalized": "min",
                }
            )
            .reset_index()
            .to_dict(orient="records")
        )

    def get_candidates_total(self) -> Dict[str, Any]:
        df = self.report_dataframe()
        return {
            "ion_ids": df.product_ion_id.nunique(),
            "putative_annotations": df.product_inchi_key.nunique(),
            "annotations_sources": df.substrate_ion_id.nunique(),
            "cosine": df.cosine.max(),
            "rule_score_normalized": df.rule_score_normalized.min(),
        }

    def get_targets(self) -> List[Target]:

        target = Target()
        targets: List[Target] = []
        df = self.gen_report_csv()
        for _, row in df.iterrows():
            if str(row.product_ion_id) != target.ion_id:
                target = Target(
                    ion_id=row.product_ion_id, mz=row.product_mz, mass=row.product_mass
                )
                targets.append(target)

            images_path = self.report_dir / "images"

            best_source = Source(
                ion_id=row.substrate_ion_id,
                name=row.substrate_name,
                smiles=row.substrate_smiles,
                images_path=images_path,
            )

            best_rule = Rule(
                rule_id=row.rule_id,
                smarts=row.rule_smarts,
                reaction_id=row.rule_reaction_id,
                ec_number=row.rule_ec_number,
                score_normalized=row.rule_score_normalized,
                images_path=images_path,
            )

            candidate = Candidate(
                smiles=row.product_smiles,
                cosine=row.cosine,
                images_path=images_path,
                ion_id=target.ion_id,
                best_rule=best_rule,
                best_source=best_source,
            )

            self.create_mol_image(best_source)
            self.create_rule_image(best_rule)
            self.create_mol_image(candidate)
            self.create_spectra_image(candidate)

            target.candidates.append(candidate)
        return targets

    def create_mol_image(self, annotation: Annotation) -> Path:
        image_path = self.report_dir / annotation.mol_image_path
        if not image_path.exists():
            Chem.Draw.MolToFile(Chem.MolFromSmiles(annotation.smiles), str(image_path))
        return image_path

    def create_rule_image(self, rule: Rule) -> Path:
        image_path = self.report_dir / rule.image_path
        if not image_path.exists():
            reaction = Chem.rdChemReactions.ReactionFromSmarts(rule.smarts)
            Chem.rdChemReactions.RemoveMappingNumbersFromReactions(reaction)
            data = Chem.Draw.ReactionToImage(reaction, useSVG=True)
            image_path.write_text(data)
        return image_path

    def create_spectra_image(self, candidate: Candidate) -> None:
        spectra_image_path = self.report_dir / candidate.spectra_image_path

        if not spectra_image_path.exists():

            exp_spectrum = self.workspace[candidate.ion_id].spectrum_set[
                str(candidate.ion_id)
            ]
            exp_spectrum = normalize_intensities(exp_spectrum)
            exp_spectrum = normalize_intensities(exp_spectrum)
            exp_spectrum = select_by_intensity(
                exp_spectrum, intensity_from=settings.spectrum_intensity_min
            )

            frag_path = (
                self.workspace.root_path
                / settings.fragmentation_folder
                / f"{candidate.inchi_key}.cfmid"
            )
            sim_spectrum = load_from_cfm_id(frag_path.read_text(), metadata={})[-1]
            sim_spectrum = normalize_intensities(sim_spectrum)

            fig = plot_spectra(exp_spectrum, sim_spectrum)
            fig.update_layout(
                showlegend=False, margin={"l": 0, "r": 5, "t": 0, "b": 0, "pad": 0}
            )
            fig.write_image(
                str(spectra_image_path), engine="kaleido", width=400, height=250
            )

    def report_dataframe(self) -> pd.DataFrame:
        if self._report_dataframe is not None:
            return self._report_dataframe
        workspace = self.workspace

        annotations_ids = list(
            read_annotations(
                self.workspace.root_path
                / getattr(workspace.__class__, "annotation").path
            ).ion_id
        )
        assessments = pd.DataFrame()
        for wf_id, workflow in workspace.items():
            logger.info("Explore and assess with ion id %s as source ...", wf_id)
            df = workflow.assessments
            if not df.empty:
                df = df[df.cosine > settings.cosine_threshold]
                df = df[~df.product_ion_id.isin(annotations_ids)]
                assessments = pd.concat([assessments, df])
            logger.info("=> %s putative annotations found", len(df))

        if assessments.empty:
            logger.info("No putatives annotations found, report will not be generated.")
            return assessments

        assessments["product_mz"] = assessments.product_mass + settings.proton_mass
        data_path = (
            self.workspace.root_path
            / settings.raw_data_folder
            / f"assessments_raw_data-D{settings.diameter}.csv"
        )
        assessments.to_csv(data_path, index=False)
        assessments["product_mz_round"] = round(assessments.product_mz, 2)

        assessments = assessments.sort_values(
            ["product_mz_round", "product_ion_id", "cosine"],
            ascending=[True, True, False],
        )

        def best_rule(rules: pd.Series) -> Any:
            def get_score(rule: Dict[str, Any]) -> float:
                return float(rule["Score_normalized"])

            return sorted(json.loads(rules), key=get_score)[0]

        assessments["best_rule"] = assessments.rules.apply(best_rule)
        assessments["best_rule_score"] = assessments.best_rule.apply(
            lambda rule: rule["Score_normalized"]
        )
        assessments = assessments.sort_values("best_rule_score")

        idx = (
            assessments.groupby(["product_inchi_key", "product_ion_id"], sort=False)[
                "best_rule_score"
            ].transform("min")
            == assessments["best_rule_score"]
        )
        assessments = assessments[idx]

        for df_key, rr_key in [
            ("smarts", "Rule_SMARTS"),
            ("score_normalized", "Score_normalized"),
            ("id", "# Rule_ID"),
            ("reaction_id", "Reaction_ID"),
            ("ec_number", "Reaction_EC_number"),
        ]:
            assessments[f"rule_{df_key}"] = assessments.best_rule.apply(
                lambda rule: rule[rr_key]
            )

        assessments = assessments.sort_values(
            ["product_mz_round", "product_ion_id", "cosine"],
            ascending=[True, True, False],
        )

        assessments = assessments.loc[
            :,
            [
                "substrate_ion_id",
                "substrate_name",
                "substrate_smiles",
                "rule_id",
                "rule_smarts",
                "rule_score_normalized",
                "rule_reaction_id",
                "rule_ec_number",
                "product_mz",
                "product_smiles",
                "product_mass",
                "product_inchi_key",
                "product_ion_id",
                "cosine",
            ],
        ].drop_duplicates()

        self._report_dataframe = assessments
        return assessments
