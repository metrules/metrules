from pathlib import Path
from typing import Any, List, Dict

import numpy as np
import pandas as pd
import ray
from matchms.Spectrum import Spectrum
from matchms.importing import load_from_mgf
from matchms.similarity import ModifiedCosine
from matchms.filtering import add_precursor_mz
from cfm_id import CfmId
from cfm_id.matchms import load_from_cfm_id
import sinagot as sg

from .settings import settings


class SpectrumSet(Dict[str, Spectrum]):
    pass


@sg.step
def get_ms1(spectrum_set: SpectrumSet) -> pd.DataFrame:
    return pd.DataFrame(
        [
            (ion_id, spectrum.metadata["pepmass"][0])
            for ion_id, spectrum in spectrum_set.items()
        ],
        columns=["ion_id", "mass"],
    )


@sg.step
def get_ms1_match(ms1: pd.DataFrame, products: pd.DataFrame) -> pd.DataFrame:
    if products.empty:
        return products
    ms1_values = ms1.mass.values
    diff = np.subtract.outer(
        products.product_mass.values,
        ms1_values,
    )
    diff = np.abs(diff)
    diff = np.abs(diff - settings.proton_mass)
    diff_ratio = diff / ms1_values
    diff_match = np.where(diff_ratio < settings.ms1_ppm_threshold * 10e-6)

    products_result = products.iloc[diff_match[0]].reset_index()
    ms1_result = ms1.iloc[diff_match[1]].reset_index()

    products_result["product_ion_id"] = ms1_result.ion_id

    return products_result


@sg.step
def frag_remote(
    ms1_match: pd.DataFrame,
    fragmentation_folder: Path,
) -> List[Any]:
    ids = []
    for _, row in ms1_match.iterrows():
        ids.append(_frag.remote(fragmentation_folder=fragmentation_folder, row=row))
    return ids


@ray.remote  # type: ignore
def _frag(fragmentation_folder: Path, row: pd.Series) -> pd.Series:
    cfm_id = CfmId(settings.cfmid_path)
    data_path = fragmentation_folder
    path = data_path / f"{row.product_inchi_key}.cfmid"
    if path.exists():
        frag_raw = path.read_text()
    else:
        frag_raw = cfm_id.predict(
            row.product_smiles, include_annotations=False, raw_format=True
        )
        path.write_text(frag_raw)
    row["frag_raw"] = frag_raw
    return row


@sg.step
def get_cosine(
    spectrum_set: pd.DataFrame,
    frag_remote_ids: List[Any],
) -> pd.DataFrame:

    cosine_engine = ModifiedCosine(intensity_power=settings.cosine_intensity_power)
    if not frag_remote_ids:
        return pd.DataFrame()
    df = pd.DataFrame(ray.get(frag_remote_ids))

    def get_cosine(row: pd.Series) -> float:
        exp_spectrum = add_precursor_mz(spectrum_set[str(row.product_ion_id)])
        frag_spectrum = load_from_cfm_id(
            row.frag_raw, {"precursor_mz": row.product_mass}
        )[-1]
        result = cosine_engine.pair(frag_spectrum, exp_spectrum)
        return float(result["score"])

    df["cosine"] = df.apply(get_cosine, axis=1)

    return df


@sg.step
def get_assessments(df: pd.DataFrame) -> pd.DataFrame:

    if "frag_raw" in df.columns:
        df = df.drop(["frag_raw"], axis=1)

    return df


class SpectrumSetHandler(sg.storage.TypeHandler, data_type=SpectrumSet):
    def read(self, path: Path) -> pd.DataFrame:
        return {
            spectrum.metadata["scans"]: spectrum
            for spectrum in list(load_from_mgf(str(path)))
        }
