import logging

from .settings import settings


logger = logging.getLogger("metrules")

logger.setLevel(settings.logging_level)
if not logger.handlers:
    logger.addHandler(logging.StreamHandler())
