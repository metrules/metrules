from pathlib import Path
from typing import Any, List, Set, Dict
import json

import numpy as np
import pandas as pd
import ray
from rdkit import Chem
from rdkit import RDLogger

from metrules.rpreactor.chemical.standardizer import Standardizer
import sinagot as sg

from .annotation import AnnotationRow
from .settings import settings

RDLogger.DisableLog("rdApp.*")


class RulesDataFrame(pd.DataFrame):  # type: ignore
    pass


class RulesHandler(sg.storage.TypeHandler, data_type=RulesDataFrame):
    def read(self, path: Path) -> pd.DataFrame:
        df = pd.read_csv(
            path,
            sep="\t",
        )
        return df


def standardize_chemical(
    rdmol: Chem.rdchem.Mol, with_hs: bool, with_stereo: bool, heavy: bool = False
) -> Any:
    """Simple standardization of RDKit molecule. (from rpreactor)"""
    params = {
        "OP_REMOVE_ISOTOPE": False,
        "OP_NEUTRALISE_CHARGE": False,
        "OP_REMOVE_STEREO": not with_stereo,
        "OP_COMMUTE_INCHI": True,
        "OP_KEEP_BIGGEST": False,
        "OP_ADD_HYDROGEN": with_hs,
        "OP_KEKULIZE": False,
        "OP_NEUTRALISE_CHARGE_LATE": True,
    }
    # if heavy:
    #     params["OP_REMOVE_ISOTOPE"] = True
    #     params["OP_NEUTRALISE_CHARGE"] = True
    #     params["OP_KEEP_BIGGEST"] = True
    return Standardizer(  # type: ignore
        sequence_fun="sequence_tunable", params=params
    ).compute(rdmol)


def run_smarts_for_smiles(reaction_smarts: str, mol_smiles: str) -> Set[str]:
    try:
        mol = Chem.MolFromSmiles(mol_smiles)
        mol = standardize_chemical(mol, with_hs=True, with_stereo=True)
        reaction = Chem.rdChemReactions.ReactionFromSmarts(reaction_smarts)
        res = reaction.RunReactants((mol,))
        return {Chem.MolToSmiles(mol) for mols in res for mol in mols}
    except Exception:
        return set()


@sg.step
def get_rules_diameter(df: pd.DataFrame) -> pd.DataFrame:
    df = df[df.Diameter == settings.diameter]
    return df


def metabolization_data_path(
    metabolization_folder: Path, annotation: AnnotationRow
) -> Path:
    return metabolization_folder / f"{annotation.inchi_key}-D{settings.diameter}.csv"


def split_rules(burn_rules: pd.DataFrame) -> Any:
    array_nums = max(1, int(len(burn_rules) / settings.rules_chunk_size))
    return np.array_split(burn_rules, array_nums)


@sg.step
def burn_rules_run(
    burn_rules: pd.DataFrame, annotation: AnnotationRow, metabolization_folder: Path
) -> List[Any]:
    path = metabolization_data_path(metabolization_folder, annotation)
    if path.exists():
        return []
    return [_burn_rules.remote(rules, annotation) for rules in split_rules(burn_rules)]


@sg.step
def burn_rules_fetch(
    burn_rules_remote: List[Any], annotation: AnnotationRow, metabolization_folder: Path
) -> pd.DataFrame:
    path = metabolization_data_path(metabolization_folder, annotation)
    try:
        df = pd.read_csv(path)
    except FileNotFoundError:
        df = pd.concat(ray.get(burn_rules_remote))
        df.to_csv(path, index=False)
    except pd.errors.EmptyDataError:
        df = pd.DataFrame()
    return df


@sg.step
def burn_rules_format(
    burn_rules_raw: pd.DataFrame, annotation: AnnotationRow
) -> pd.DataFrame:

    df = burn_rules_raw
    if df.empty:
        return df

    RULE_COLUMNS = [
        "# Rule_ID",
        "Legacy_ID",
        "Reaction_ID",
        "Diameter",
        "Rule_order",
        "Rule_SMARTS",
        "Score",
        "Score_normalized",
        "Reaction_EC_number",
        "Reaction_direction",
        "Rule_relative_direction",
        "Rule_usage",
    ]

    def get_rule_as_dict(row: pd.Series) -> Dict[str, Any]:
        return {key: row[key] for key in RULE_COLUMNS}

    df["rule"] = df.apply(get_rule_as_dict, axis=1)

    def get_mass(row: pd.Series) -> float:
        mol = Chem.MolFromSmiles(row.product_smiles)
        return float(Chem.Descriptors.ExactMolWt(mol))

    df["product_mass"] = df.apply(get_mass, axis=1)

    def get_inchi_key(row: pd.Series) -> str:
        mol = Chem.MolFromSmiles(row.product_smiles)
        return str(Chem.MolToInchiKey(mol))

    df["product_inchi_key"] = df.apply(get_inchi_key, axis=1)

    df["substrate_ion_id"] = annotation.ion_id
    df["substrate_name"] = annotation.name

    COLUMNS = [
        "rule",
        "substrate_ion_id",
        "substrate_smiles",
        "substrate_name",
        "product_smiles",
        "product_mass",
        "product_inchi_key",
    ]

    df = df.loc[
        :,
        COLUMNS,
    ]

    df = df.groupby(COLUMNS[1:])[COLUMNS[0]].apply(list).reset_index(name="rules")
    df["rules"] = df["rules"].apply(json.dumps)
    return df


@ray.remote  # type: ignore
def _burn_rules(rules: pd.DataFrame, annotation: AnnotationRow) -> pd.DataFrame:
    result = pd.DataFrame()
    for _, row in rules.iterrows():
        products = run_smarts_for_smiles(row.Rule_SMARTS, annotation.smiles)
        if products:
            products = set(r_ for r in products for r_ in r.split("."))
            df_products = pd.DataFrame(
                [
                    pd.concat(
                        [
                            row,
                            pd.Series(
                                {
                                    "substrate_smiles": annotation.smiles,
                                    "product_smiles": product,
                                }
                            ),
                        ]
                    )
                    for product in products
                ]
            )
            result = pd.concat([result, df_products])
    return result
