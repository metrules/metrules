from pathlib import Path

from pydantic import BaseSettings


class Settings(BaseSettings):

    logging_level: str = "INFO"

    inputs_folder = "inputs"
    rules_folder = "intermediate_data/rules"
    metabolization_folder = "metabolization"
    fragmentation_folder = "fragmentation"
    intermedidate_data_folder = "intermediate_data"
    reports_folder = "reports"
    raw_data_folder = "raw_data"

    rules_path: Path
    rules_chunk_size: int = 1000
    diameter: int

    proton_mass: float = 1.0072765

    ms1_ppm_threshold: float = 5.0
    cfmid_path: Path
    spectrum_intensity_min: float = 0.05
    cosine_intensity_power: float = 0.5
    cosine_threshold: float = 0.3

    report_md: bool = False
    report_csv: bool = False

    class Config:
        env_prefix = "metrules_"


settings = Settings()
