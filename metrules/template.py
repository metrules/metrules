from pathlib import Path
from typing import Any

from jinja2 import Template
from md2pdf.core import md2pdf


class TemplateEngine:
    @property
    def template_dir_path(self) -> Path:
        return Path(__file__).parent / "templates"

    def gen_md_content(self, template_name: str, **kwargs: Any) -> str:
        template_path = self.template_dir_path / f"{template_name}.md"
        template = Template(template_path.read_text())
        return template.render(**kwargs)

    def gen_pdf(
        self, template_name: str, dir_path: Path, write_md: bool = False, **kwargs: Any
    ) -> None:
        md_content = self.gen_md_content(template_name=template_name, **kwargs)
        if write_md:
            (dir_path / f"{template_name}.md").write_text(md_content)
        md2pdf(
            str(dir_path / f"{template_name}.pdf"),
            md_content=md_content,
        )
