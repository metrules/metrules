# MetRules Data

## Content

This folder contains processed [MetRules](https://gitlab.com/metrules/metrules) Data.

## Data Structure

<pre>
├── inputs/
│   └── spectrum_set.mgf
│   └── annotations.csv
├── metabolization/
│   └── [inchi-key]-D[diameter].csv
│   └── ...
├── fragmentation/
│   └── [inchi-key].cfmid
│   └── ...
├── reports/
│   └── Assessment Report - D[diameter].pdf
│   └── ...
└── raw_data/
    └── assessments_raw_data-D[diameter].csv
    └── ...
</pre>

## inputs/

Input data, contains MS/MS data in `.mgf` format and initial annotations in `.csv` format.

## metabolization/

[RetroRules](https://retrorules.org/) _in silico_ metabolization results.

## fragmentation/

[CFM-ID](https://cfmid.wishartlab.com/) 2.0 _in silico_ fragmentation results.

## reports/

Assessment reports for each RetroRules diameter, using [Matchms](https://matchms.readthedocs.io/en/latest/) similarity cosine.

### raw_data/

Raw data of assessment report.
