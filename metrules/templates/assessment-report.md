# MetRules Assessment Report

Putative annotations assessment report. For each spectra plot, blue is experimental spectrum and red is <i>in silico</i> spectrum.

### Settings

- MetRules version : {{ metrules_version }}
- source code: [https://gitlab.com/metrules/metrules/-/tree/v{{ metrules_version }}](https://gitlab.com/metrules/metrules/-/tree/v{{ metrules_version }})
- [RetroRules](https://retrorules.org/) diameter : {{ settings.diameter }}
- MS1 match PPM threshold : {{ settings.ms1_ppm_threshold }}
- MS2 similarity (cosine) match threshold : {{ settings.cosine_threshold }}

## Putative Annotations Synthesis

<table>
    <tr>
        <th>m/Z</th>
        <th>Ion Id</th>
        <th>putative annotations</th>
        <th>annotations sources</th>
        <th>Best cosine</th>
        <th>Best rule score</th>
    </tr>
    {% for candidate in candidates_row -%}
    <tr>
        <th>{{ candidate["product_mz_round"]|round(4) }}</th>
        <th>{{ candidate["product_ion_id"] }}</th>
        <td>{{ candidate["product_inchi_key"] }}</td>
        <td>{{ candidate["substrate_ion_id"] }}</td>
        <td>{{ candidate["cosine"]|round(2) }}</td>
        <td>{{ candidate["rule_score_normalized"]|round(2) }}</td>
    </tr>
    {% endfor %}
    <tr>
        <th> Total </th>
        <th>{{ candidates_total["ion_ids"] }}</td>
        <th>{{ candidates_total["putative_annotations"] }}</td>
        <th>{{ candidates_total["annotations_sources"] }}</td>
        <th>{{ candidates_total["cosine"]|round(2) }}</td>
        <th>{{ candidates_total["rule_score_normalized"]|round(2) }}</td>
    </tr>
</table>

## Putative Annotations Details

{% for target in targets -%}

#### m/z: {{ target.mz|round(4) }} | ion ID: {{ target.ion_id }}

{% for candidate in target.candidates -%}

<table>
    <tr>
        <td>
            <p>
                <a class="inchi_key align-top" href="https://lotus.naturalproducts.net/search/simple/{{ candidate.inchi_key }}">
                    {{ candidate.inchi_key }}</a>
            </p>
            <img class="mol" src="{{ candidate.mol_image_path }}" alt="Molecule image not available">
        </td>
        <td>
            <p>cosine: {{ candidate.cosine|round(2) }}</p>
            <img class="spectra" src="{{ candidate.spectra_image_path }}" alt="Spectra image not available">
        </td>
    </tr>
    <tr>
        <td>
            <p class="small">Best source</p>
            <img class="mol-source" src="{{ candidate.best_source.mol_image_path }}" alt="Spectra image not available">
            <p class="small">
                <a class="inchi_key align-top" href="https://lotus.naturalproducts.net/search/simple/{{ candidate.best_source.inchi_key }}">
                {{ candidate.best_source.name }}</a>, ion Id: {{ candidate.best_source.ion_id }}
            </p>
        </td>
        <td>
            <p class="small">Best rule</p>
            <img class="rule" src="{{ candidate.best_rule.image_path }}" alt="Molecule image not available">
            <p class="small">
                {% if candidate.best_rule.ec_number_match %}
                    EC:<a href="https://www.brenda-enzymes.org/enzyme.php?ecno={{ candidate.best_rule.ec_number }}">{{ candidate.best_rule.ec_number }}</a>,
                {% else %}
                    MetaNetX:<a href="https://www.metanetx.org/equa_info/{{ candidate.best_rule.reaction_id }}">{{ candidate.best_rule.reaction_id }}</a>,
                {% endif %}
                penalty score: {{ candidate.best_rule.score_normalized|round(2) }}</p>
            <br />
        </td>
    </tr>

</table>
{% endfor %}
<div style="page-break-after: always;"></div>
{% endfor %}
