from typing import Any, List
from pathlib import Path

import pandas as pd
import sinagot as sg

from .annotation import AnnotationRow
from .metabolization import (
    RulesDataFrame,
    get_rules_diameter,
    burn_rules_run,
    burn_rules_fetch,
    burn_rules_format,
)
from .fragmentation import (
    SpectrumSet,
    get_ms1,
    get_ms1_match,
    frag_remote,
    get_cosine,
    get_assessments,
)


class MetRulesWorkflow(sg.Workflow):
    rules_all: RulesDataFrame = sg.seed()
    rules_diameter: pd.DataFrame = get_rules_diameter.step(rules_all)

    spectrum_set: SpectrumSet = sg.seed()
    ms1: pd.DataFrame = get_ms1.step(spectrum_set)
    annotation: AnnotationRow = sg.seed()

    metabolization_folder: Path = sg.seed()
    burn_rules_remote: List[Any] = burn_rules_run.step(
        burn_rules=rules_diameter,
        annotation=annotation,
        metabolization_folder=metabolization_folder,
    )
    burn_rules_raw: pd.DataFrame = burn_rules_fetch.step(
        burn_rules_remote=burn_rules_remote,
        annotation=annotation,
        metabolization_folder=metabolization_folder,
    )
    products: pd.DataFrame = burn_rules_format.step(
        burn_rules_raw=burn_rules_raw, annotation=annotation
    )

    ms1_match: pd.DataFrame = get_ms1_match.step(ms1=ms1, products=products)
    fragmentation_folder: Path = sg.seed()
    frag_remote_ids: List[Any] = frag_remote.step(
        ms1_match=ms1_match, fragmentation_folder=fragmentation_folder
    )

    cosines: pd.DataFrame = get_cosine.step(
        spectrum_set=spectrum_set,
        frag_remote_ids=frag_remote_ids,
    )
    assessments: pd.DataFrame = get_assessments.step(cosines)
