from typing import Any
from pathlib import Path
import shutil

import sinagot as sg

from .annotation import LocalAnnotationStorage
from .settings import settings
from .workflow import MetRulesWorkflow
from .assessment import Assessment
from .template import TemplateEngine


class MetRulesWorkspace(sg.Workspace[MetRulesWorkflow]):
    def __init__(self, *args: Any, **kwargs: Any):

        super().__init__(*args, **kwargs)
        self.create_readme()
        self.create_folders()
        self.set_input_name(".mgf", "spectrum_set")
        self.set_input_name(".csv", "annotation")

    rules_all = sg.LocalStorage(settings.rules_path)
    rules_diameter = sg.LocalStorage(
        f"{settings.rules_folder}/D{settings.diameter}.csv",
        write_kwargs={"index": False},
    )

    spectrum_set = sg.LocalStorage(f"{settings.inputs_folder}/spectrum_set.mgf")
    ms1 = sg.LocalStorage(
        f"{settings.intermedidate_data_folder}/ms1.csv",
        write_kwargs={"index": False},
    )
    annotation = LocalAnnotationStorage(f"{settings.inputs_folder}/annotations.csv")

    metabolization_folder = sg.LocalStorage(settings.metabolization_folder)
    fragmentation_folder = sg.LocalStorage(settings.fragmentation_folder)

    @property
    def assessment(self) -> Assessment:
        return Assessment(self)

    def create_readme(self) -> None:
        TemplateEngine().gen_pdf(
            template_name="README",
            dir_path=self.root_path,
            write_md=True,
        )

    def create_folders(self) -> None:
        for folder in (
            settings.rules_folder,
            settings.metabolization_folder,
            settings.fragmentation_folder,
            settings.inputs_folder,
            settings.intermedidate_data_folder,
            f"{settings.reports_folder}/images",
            settings.raw_data_folder,
        ):
            path = self.root_path / folder
            if not path.exists():
                path.mkdir(parents=True, exist_ok=True)

    def set_input_name(self, suffix: str, attr_name: str) -> None:
        mgf_glob = list(Path(self.root_path, "inputs").glob(f"*{suffix}"))
        if len(mgf_glob) == 0:
            raise DataInputError(
                f"Missing {suffix} file in {settings.inputs_folder} folder"
            )
        if len(mgf_glob) > 1:
            raise DataInputError(
                f"Need only one {suffix} file in {settings.inputs_folder} folder"
            )
        mgf_name = mgf_glob[0].name
        storage_class = getattr(self, attr_name).__class__
        setattr(
            self.__class__,
            attr_name,
            storage_class(f"{settings.inputs_folder}/{mgf_name}"),
        )

    def remove_intermediate_folders(self) -> None:
        for folder in (settings.intermedidate_data_folder,):
            path = self.root_path / folder
            shutil.rmtree(path)


class DataInputError(Exception):
    pass
