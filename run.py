import sys
from pathlib import Path

from metrules import MetRulesWorkspace
from metrules.logger import logger

data_path = Path(sys.argv[1])

ws = MetRulesWorkspace(data_path)

logger.info("Generating assessment report ...")

ws.assessment.gen_report_pdf(force=True)
ws.remove_intermediate_folders()

logger.info("Assessment report generated")
