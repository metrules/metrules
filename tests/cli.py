import subprocess
import shlex


def run() -> None:
    print("### Run Flake8 ###")
    cmd = "flake8 metrules tests --extend-exclude metrules/rpreactor"
    subprocess.run(shlex.split(cmd))

    print("### Run MyPy ###")
    cmd = "mypy --strict --exclude metrules/rpreactor metrules tests"
    subprocess.run(shlex.split(cmd))

    print("### Run pytest ###")
    cmd = "pytest --cov-report term-missing --cov=metrules --disable-warnings"
    subprocess.run(shlex.split(cmd))
