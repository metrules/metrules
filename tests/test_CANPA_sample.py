import os
from pathlib import Path

import pytest


@pytest.fixture
def CANPA_dir(shared_datadir: Path) -> Path:
    return shared_datadir / "CANPA_sample"


@pytest.fixture
def set_env(shared_datadir: Path, CANPA_dir: Path) -> None:

    os.environ["METRULES_CFMID_PATH"] = str(
        (Path(__file__).parents[1] / "vendor" / "cfm_id").resolve()
    )
    os.environ["METRULES_RULES_PATH"] = str(CANPA_dir / "rules_sample.tsv")
    os.environ["METRULES_DIAMETER"] = "14"
    os.environ["METRULES_REPORT_CSV"] = "true"
    os.environ["METRULES_REPORT_MD"] = "true"


def test_CANPA_sample_with_results(set_env: None, CANPA_dir: Path) -> None:

    from metrules import MetRulesWorkspace

    ws = MetRulesWorkspace(CANPA_dir / "tested_data")
    ws.assessment.gen_report_pdf()

    tested_path = CANPA_dir / "tested_data"

    tested_report_data = tested_path / "raw_data" / "assessments_raw_data-D14.csv"
    expected_report_data = (
        CANPA_dir / "expected_data" / "raw_data" / "assessments_raw_data-D14.csv"
    )

    assert tested_report_data.exists()
    assert tested_report_data.read_text() == expected_report_data.read_text()

    assert (tested_path / "reports" / "Assessment Report - D14.pdf").exists()

    ws.remove_intermediate_folders()


def test_CANPA_sample_no_results(set_env: None, CANPA_dir: Path) -> None:

    from metrules import MetRulesWorkspace

    ws = MetRulesWorkspace(CANPA_dir / "no_result_data")
    ws.assessment.gen_report_pdf()

    tested_reports_path = CANPA_dir / "tested_data" / "reports"
    assert not (tested_reports_path / "Assessment Report - D14.pdf").exists()
