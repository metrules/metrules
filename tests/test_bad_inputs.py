from pathlib import Path

import pytest


def test_bad_inputs(shared_datadir: Path) -> None:
    from metrules import MetRulesWorkspace
    from metrules.workspace import DataInputError

    for path in (shared_datadir / "bad_inputs").iterdir():
        with pytest.raises(DataInputError):
            MetRulesWorkspace(path)
